set nocompatible              " be iMproved, required
filetype off                  " required
 
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'Shougo/vimproc.vim'
Plugin 'scrooloose/nerdtree'
" You need to run make file in vimproc

call vundle#end()            " required
filetype plugin indent on    " required

:nnoremap <leader>th :tabp<CR>
:nnoremap <leader>tl :tabn<CR>
:nnoremap <leader>rr<Up> :exe "resize +10"<CR>
:nnoremap <leader>rr<Down> :exe "resize -10"<CR>
:nnoremap <leader>rr<Right> :exe "vertical resize +10"<CR>
:nnoremap <leader>rr<Left> :exe "vertical resize -10"<CR>
:nnoremap <leader>r<Up> :exe "resize +5"<CR>
:nnoremap <leader>r<Down> :exe "resize -5"<CR>
:nnoremap <leader>r<Right> :exe "vertical resize +5"<CR>
:nnoremap <leader>r<Left> :exe "vertical resize -5"<CR>
:inoremap jj <Esc>

:let mapleader = ","
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:nnoremap <leader>ec :vsplit $HOME/vimcheat<cr>

:nnoremap <leader>cb :!cabal build -v0 --ghc-options="-Wall"<cr>
:nnoremap <leader>ct :!cabal test<cr>
:nnoremap <leader>hi :!ghci %<cr>
:nnoremap <leader>hb :!ghc %<cr>
:nnoremap <leader>gs :!git status %<cr>

au FileType haskell set tabstop=8 expandtab softtabstop=4 shiftwidth=4 shiftround
au FileType lhaskell set tabstop=8 expandtab softtabstop=4 shiftwidth=4 shiftround
au VimEnter * NERDTree
au VimEnter * wincmd p

:set nu
:set shortmess+=A
:set nocp
:set nowrap

"au BufNewFile,BufRead *.lhs set filetype=haskell
