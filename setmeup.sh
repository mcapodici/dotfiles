#!/bin/bash

if [ "$(id -u)" == "0" ]; then
	echo "Don't run me as root. I sudo when I need to!"
	exit 1
fi

ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
sudo chsh -s /bin/bash $USER
cd ~/
sudo apt-get -y update
sudo apt-get -y install vim-gtk git haskell-platform
mv .bashrc .bashrc_original
mv .profile .profile_original
git init .
git remote add -t \* -f origin https://mcapodici@bitbucket.org/mcapodici/dotfiles.git
git checkout master
source ~/.bashrc
source ~/.profile
cabal update
cabal install Cabal cabal-install
if [ ! -d "~/.vim/bundle/Vundle.vim" ]; then
	git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	vim +PluginInstall +qall
fi